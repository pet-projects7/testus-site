# frozen_string_literal: true

module Panel
  class PanelController < ApplicationController
    layout "panel"

    before_action :authenticate_user!

    def home; end

    def lists; end
  end
end
