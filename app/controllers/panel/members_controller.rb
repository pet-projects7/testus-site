# frozen_string_literal: true

module Panel
  class MembersController < PanelController
    before_action :set_project
    before_action :set_members

    def index; end

    def create
      email = params[:user][:email]
      name = params[:user][:name]
      @user_invite_command = UserInviteCommand.call(email, name, @project)

      render "panel/members/index"
    end

    private

    def set_project
      @project = Project.friendly.find(params[:project_id])
    end

    def set_members
      @members = @project.users
    end
  end
end
