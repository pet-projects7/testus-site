# frozen_string_literal: true

module Panel
  class ScenariosController < PanelController
    skip_before_action :authenticate_user!

    # rubocop:disable Metrics/MethodLength
    def index
      # Only numbers are allowed for feature_id params.
      feature = Feature.find_by(id: params["feature_id"])

      project = feature.project

      view_variables = {
        feature: feature,
        scenarios: params[:status] ?
                       feature.scenarios.with_status(params[:status]) :
                       feature.scenarios
      }

      if current_user || project.public || project.is_a?(DemoProject)
        return render partial: "panel/projects/scenarios",
                      locals: view_variables
      end

      raise ActionController::Forbidden
    end
    # rubocop:enable Metrics/MethodLength
  end
end
