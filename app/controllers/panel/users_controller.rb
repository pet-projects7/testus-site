# frozen_string_literal: true

module Panel
  class UsersController < PanelController
    def profile; end
  end
end
