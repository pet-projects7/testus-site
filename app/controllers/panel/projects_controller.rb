# frozen_string_literal: true

module Panel
  class ProjectsController < PanelController
    include ReportSearchable

    prepend_before_action :set_project, only: %i[update
                                                 show
                                                 edit
                                                 settings
                                                 documentation
                                                 history]

    def index
      @projects = current_user.projects.includes(:test_reports)
    end

    def update
      command_response = SaveProjectSettingsCommand.call(@project,
                                                         current_user.id,
                                                         project_params)

      redirect_to panel_project_settings_path(@project) if command_response.success?
    end

    def settings
      @user_project_settings = current_user
                                .user_project_settings
                                .where(project: @project)
    end

    def new
      @project = Project.new
    end

    def create
      command_response = CreateProjectCommand.call(project_params,
                                                   current_user)

      if command_response.success?
        redirect_to action: :index, notice: "Project was successfully updated."
      else
        render :new
      end
    end

    def history
      @test_reports = @project.test_reports
                              .limit(30)
                              .order(created_at: :desc)
    end

    def edit; end

    def project_params
      params.require(:project).permit(:title, :description, :public,
                                      { user_project_settings: [:notify_on_failures] })
    end

    def set_project
      @project = current_user.projects.friendly.find(params[:id] || params[:project_id])
    end
  end
end
