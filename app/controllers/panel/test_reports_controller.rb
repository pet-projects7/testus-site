# frozen_string_literal: true

module Panel
  class TestReportsController < PanelController
    before_action :set_project, only: %i[new status show]
    before_action :set_test_report, only: %i[status show]

    def new; end

    def show
      @documentation_facade = Panel::Projects::ShowTestReportFacade.new(@project, @test_report)
      render "panel/projects/show"
    end

    def status
      render json: {
        data: { status: @test_report.test_report_process.status }
      }
    end

    private

    def set_project
      @project = Project.friendly.find(params[:project_id])
    end

    def set_test_report
      @test_report = @project.test_reports.find(params[:test_report_id] || params[:id])
    end
  end
end
