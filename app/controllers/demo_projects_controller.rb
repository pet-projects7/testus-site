# frozen_string_literal: true

class DemoProjectsController < ApplicationController
  before_action :set_project, only: [:show]

  def index
    @form_model = DemoProject.new
    @form_model.test_reports.build
  end

  def show
    return render "public_projects/access_restricted" unless @project.present? && @project.is_a?(DemoProject)

    @documentation_facade = Panel::Projects::DocumentationFacade.new(@project, params)
  end

  def create
    demo_project = DemoProject.create new_demo_project_params
    test_report = demo_project.test_reports.last

    ParseReportWorker.perform_async(test_report.id)

    redirect_to demo_project_path(demo_project)

    # return render json: { success: true, data: {  } }
  end

  private

  def set_project
    @project = Project.friendly.find(params[:id])
  end

  def new_demo_project_params
    params.require(:demo_project).permit(:title, { test_reports_attributes: %i[report_file source] })
  end
end
