# frozen_string_literal: true

class PublicProjectsController < ApplicationController
  before_action :set_project, only: [:show]

  def index
    @public_projects = Project.where(public: true)
  end

  def show
    return render "public_projects/access_restricted" if @project.blank? || !@project.public?

    @documentation_facade = Panel::Projects::DocumentationFacade.new(@project, params)
  end

  private

  def set_project
    @project = Project.friendly.find(params[:id])
  end
end
