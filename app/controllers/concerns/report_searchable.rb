# frozen_string_literal: true

module ReportSearchable
  extend ActiveSupport::Concern

  def show
    @documentation_facade = Panel::Projects::DocumentationFacade.new(@project, filter_params)
  end

  private

  def filter_params
    params.permit(filter: %i[status tags])
  end
end
