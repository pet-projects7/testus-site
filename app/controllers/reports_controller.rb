# frozen_string_literal: true

class ReportsController < ApplicationController
  protect_from_forgery except: :create

  def new; end

  def create
    project = find_project
    return render json: { error: "Project not found" } if project.nil?

    test_report = project.test_reports.create report_file: params[:report], source: :external
    ParseReportWorker.perform_async(test_report.id)
    render json: { success: true, data: { reportPath: panel_project_path(project) } }
  end

  private

  def find_project
    Project.find_by(token: params[:token])
  end
end
