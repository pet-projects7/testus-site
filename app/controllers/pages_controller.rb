# frozen_string_literal: true

class PagesController < ApplicationController
  def show
    raise StandardError, "unexpected partial request: #{params[:id]}" unless %w[instructions about].include? params[:id]

    render "pages/#{params[:id]}"
  end
end
