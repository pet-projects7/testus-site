# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale

  def select_locale
    cookies[:lang] = params[:lang] if I18n.locale_available?(params[:lang])
    redirect_to request.referer
  end

  def default_url_options
    { lang: I18n.locale }
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :password) }
    devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:name, :email, :password, :current_password) }
  end

  def set_locale
    locale = cookies[:lang] || (I18n.locale_available?(params[:lang]) ? params[:lang] : I18n.default_locale)
    I18n.locale = locale
  end
end
