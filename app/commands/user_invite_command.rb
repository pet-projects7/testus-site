# frozen_string_literal: true

class UserInviteCommand < BaseCommand
  private

  def initialize(email, name, project) # rubocop:disable Lint/MissingSuper
    @email = email
    @name = name
    @project = project
  end

  def payload
    set_user

    errors.add(:base, "User already added to this project") if user_exists? && user_already_invited_to_project?

    invite_user_to_project if user_exists? && !user_already_invited_to_project?

    return if user_exists?

    create_new
    invite_user_to_project
  end

  def invite_user_to_project
    @project.users << @user
    UserInvitedToProjectMailer.with(project: @project, user: @user).invite.deliver_now
  end

  def set_user
    @user = User.find_by(email: @email)
  end

  def create_new
    generated_password = Devise.friendly_token.first(8)
    @user = User.create!(email: @email, password: generated_password, name: @name)
  end

  def user_exists?
    @user.present?
  end

  def user_already_invited_to_project?
    @project.users.find_by(id: @user.id)
  end
end
