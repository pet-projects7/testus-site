# frozen_string_literal: true

class SaveProjectSettingsCommand < BaseCommand
  def initialize(project, user_id, params)
    @params = params.to_h
    @project = project
    @user_id = user_id
    @user_project_settings = @params.delete("user_project_settings")
  end

  def payload
    save_project
    save_project_settings
  end

  private

  def save_project
    @project.update @params
  end

  def save_project_settings
    @user_project_settings.each do |name, value|
      if value.to_i == 1
        @project.user_project_settings.first_or_create(name: name, user_id: @user_id)
      else
        @project.user_project_settings.find_by(name: name, user_id: @user_id)&.destroy
      end
    end
  end
end
