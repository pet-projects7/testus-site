# frozen_string_literal: true

class ParseReportCommand < BaseCommand
  private

  def initialize(features) # rubocop:disable Lint/MissingSuper
    @features = features
  end

  def payload
    # return unless report_schema_valid?

    @result = {}
    @result[:features] = fetch_features

    @result[:features] = remove_duplicates_among_features(@result[:features])
    @result[:features] = fetch_feature_statuses(@result[:features])

    @result[:test_report] = {}
    @result[:test_report][:tags] = @result[:features].pluck(:tags).flatten
    @result[:test_report][:status] = @result[:features].pluck(:status).flatten
  end

  def fetch_features
    @features.map { |feature| fetch_feature(feature) }
  end

  def report_schema_valid?
    is_valid = @features
               .map { |feature| Report::FeatureSchema.call(feature) }
               .map { |parsed_schema| parsed_schema&.errors&.empty? }
               .all?(true)

    return true if is_valid

    errors.add(:report, "Schema of report is invalid")
    false
  end

  def fetch_feature(feature)
    {
      uri: feature["uri"],
      cucumber_id: feature["id"],
      keyword: feature["keyword"],
      name: feature["name"],
      description: feature["description"],
      status: feature["status"],
      scenarios: fetch_scenarios(feature["elements"]),
      tags: feature.fetch("tags", []).pluck("name")
    }
  end

  def fetch_feature_statuses(features)
    # FIXME: Не оптимально, так как сначала собирает у всех статусы в длинный массив

    features.map do |feature|
      feature[:status] = feature[:scenarios].pluck(:status).flatten.uniq
      feature
    end
  end

  def fetch_scenarios(scenarios) # rubocop:disable Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/AbcSize, Metrics/PerceivedComplexity
    processed_scenarios = []

    scenarios.each_with_index do |element, index|
      next if element["type"] == "background"

      result = {
        id: element["id"],
        keyword: element["keyword"],
        name: element["name"],
        description: element["description"],
        line: element["line"],
        cucumber_type: element["type"],
        after: element["after"],
        steps: element["steps"].each { |step| fetch_step(step) },
        duration: element["steps"]
               .filter { |step| step.dig("result", "duration") }
               .map { |step| step["result"]["duration"].to_i }
               .sum,
        tags: element.fetch("tags", []).pluck("name")
      }

      if scenarios[index - 1].present? && scenarios[index - 1]["type"] == "background"
        result[:background] = fetch_background(scenarios[index - 1])
        result[:steps] = result[:background][:steps] + result[:steps]
        result[:duration] += result[:duration].to_i + result[:background][:steps]
                             .filter { |step| step.dig("result", "duration") }
                             .map { |step| step["result"]["duration"].to_i }
                             .sum
      end

      result[:status] = result[:steps].map { |step| step["result"]["status"] }.uniq

      processed_scenarios << result
    end
    # rubocop:enable

    processed_scenarios
  end

  def fetch_background(background)
    {
      keyword: background["keyword"],
      name: background["name"],
      description: background["description"],
      line: background["line"],
      type: background["type"],
      before: background["before"],
      steps: background["steps"].each { |step| fetch_step(step) }
    }
  end

  def fetch_step(step)
    {
      keyword: step["keyword"],
      name: step["name"],
      line: step["line"],
      location: step["match"]["location"],
      result: {
        status: [step["result"]["status"]],
        duration: step["result"]["duration"]
      }
    }
  end

  def remove_duplicates_among_features(features)
    aggregated_features = {}

    features.each do |feature|
      feature_index = aggregated_features[feature[:name]]

      if feature_index.present?
        feature_index[:scenarios] += feature[:scenarios]
      else
        aggregated_features[feature[:name]] = feature
      end
    end

    aggregated_features.values
  end
end
