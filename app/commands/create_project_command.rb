# frozen_string_literal: true

class CreateProjectCommand < BaseCommand
  def initialize(project_params, current_user) # rubocop:disable Lint/MissingSuper
    @project_params = project_params
    @current_user = current_user
    @user_id = @current_user.id
    @user_project_settings = @project_params.delete("user_project_settings")
  end

  def payload
    @result = {}

    @result[:project] = create_project
    save_project_settings
  end

  private

  def create_project
    @current_user.projects.create @project_params
  end

  def save_project_settings
    @user_project_settings.each do |name, value|
      if value.to_i == 1
        @result[:project].user_project_settings.first_or_create(name: name, user_id: @user_id)
      else
        @result[:project].user_project_settings.find_by(name: name, user_id: @user_id)&.destroy
      end
    end
  end
end
