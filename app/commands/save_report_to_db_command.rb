# frozen_string_literal: true

class SaveReportToDbCommand < BaseCommand
  private

  attr_reader :feature, :test_report, :saved_feature

  def initialize(feature, test_report) # rubocop:disable Lint/MissingSuper
    @feature = feature
    @test_report = test_report
  end

  def payload
    save_features
    save_scenarios
    save_total_feature_duration

    @saved_feature
  end

  def save_features
    feature_params = {
      uri: feature[:uri],
      cucumber_id: feature[:id],
      keyword: feature[:keyword],
      name: feature[:name],
      description: feature[:description],
      status: feature[:status],
      tags: feature[:tags].map { |tag_name| Tag.find_or_create_by(title: tag_name) }
    }

    @saved_feature = test_report.features.create feature_params
  end

  # rubocop:disable Metrics/MethodLength
  def save_scenarios
    creation_params = feature[:scenarios].map do |scenario|
      {
        cucumber_id: scenario[:cucumber_id],
        keyword: scenario[:keyword],
        name: scenario[:name],
        description: scenario[:description],
        line: scenario[:line],
        cucumber_type: scenario[:type],
        steps: fetch_steps(scenario[:steps]),
        duration: scenario[:duration],
        status: scenario[:status],
        tags: scenario[:tags].map { |tag_name| Tag.find_or_create_by(title: tag_name) }
      }
    end

    @saved_feature.scenarios.create creation_params
  end
  # rubocop:enable Metrics/MethodLength

  # rubocop:disable Metrics/MethodLength
  def fetch_steps(steps)
    return [] if steps.nil?

    result = []
    result = steps.map do |step|
      {
        # cucumber_type: step['test'],
        keyword: step["keyword"],
        name: step["name"],
        line: step["line"],
        location: step["location"],
        status: step["result"]["status"],
        duration: step["result"]["duration"]
      }
    end

    Step.create result
  end
  # rubocop:enable Metrics/MethodLength

  def save_total_feature_duration
    feature_duration = @saved_feature.scenarios.pluck("duration").map(&:to_i).sum

    @saved_feature.update(duration: feature_duration)
  end
end
