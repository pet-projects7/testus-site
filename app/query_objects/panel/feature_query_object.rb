# frozen_string_literal: true

module Panel
  class FeatureQueryObject
    def initialize(initial_scope)
      @initial_scope = initial_scope
    end

    def search(params)
      local_scope = @initial_scope

      local_scope = by_status(local_scope, params[:status])
      by_tags(local_scope, params[:tags])
    end

    private def by_status(scope, status)
      status ? scope.with_status(status) : scope
    end

    private def by_tags(scope, tags)
      tags ? scope.includes(:tags).where(tags: { id: [tags] }) : scope
    end
  end
end
