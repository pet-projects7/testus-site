# frozen_string_literal: true

class Tagging < ApplicationRecord
  self.table_name = "tagging"

  belongs_to :tag
  belongs_to :taggable, polymorphic: true
end
