# frozen_string_literal: true

class Feature < ApplicationRecord
  include AvailableStatuses
  include Sluggable
  include Taggable

  belongs_to :test_report
  has_many :scenarios, dependent: :destroy
  # has_and_belongs_to_many :tags
  delegate :project, to: :test_report

  def slug_candidates
    [:name]
  end
end
