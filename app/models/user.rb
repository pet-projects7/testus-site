# frozen_string_literal: true

class User < ApplicationRecord
  validates :name, presence: true

  has_and_belongs_to_many :projects
  has_many :test_reports, through: :projects
  has_many :features, through: :test_reports
  has_many :user_project_settings

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
end
