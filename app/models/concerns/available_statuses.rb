# frozen_string_literal: true

module AvailableStatuses
  extend ActiveSupport::Concern

  included do
    extend ArrayEnum

    array_enum status: { passed: 1, pending: 2, skipped: 3, undefined: 4, failed: 5 }, array: true
  end
end
