# frozen_string_literal: true

require "babosa"

module Sluggable
  extend ActiveSupport::Concern

  included do
    include FriendlyId
    friendly_id :slug_candidates, use: [:slugged]

    def normalize_friendly_id(text)
      text.to_slug.normalize! transliterations: %i[russian latin]
    end
  end
end
