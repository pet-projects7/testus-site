# frozen_string_literal: true

class UserProjectSetting < ApplicationRecord
  enum name: { notify_on_failures: 0 }

  belongs_to :project
  belongs_to :user
end
