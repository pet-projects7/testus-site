# frozen_string_literal: true

class Project < ApplicationRecord
  # Concerns
  include Sluggable

  # Callbacks
  after_create :create_token

  # Associations
  has_many :test_reports, dependent: :destroy
  has_many :user_project_settings, dependent: :destroy

  has_and_belongs_to_many :users

  # Validations
  validates :title, presence: true

  # Nested attributes
  accepts_nested_attributes_for :test_reports
  accepts_nested_attributes_for :user_project_settings

  def users_to_notify
    UserProjectSetting.notify_on_failures.where(project: self)
  end

  private

  def create_token
    update(token: to_sgid(expires: nil, for: Time.zone.now.to_s).to_s)
  end

  def slug_candidates
    [:title]
  end
end
