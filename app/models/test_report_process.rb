# frozen_string_literal: true

class TestReportProcess < ApplicationRecord
  belongs_to :test_report
  enum status: { created: 1, processing: 2, processed_with_error: 3, processed_with_success: 4 }
end
