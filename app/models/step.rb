# frozen_string_literal: true

class Step < ApplicationRecord
  include AvailableStatuses

  belongs_to :scenario
  enum cucumber_type: { background: 1, test: 2 }
end
