# frozen_string_literal: true

class TestReport < ApplicationRecord
  enum source: { manual: 0, external: 1 }
  include AvailableStatuses
  include Taggable

  after_create :create_test_report_status

  has_one_attached :report_file, dependent: :destroy
  belongs_to :project
  has_many :features, dependent: :destroy
  has_many :scenarios, through: :features
  has_one :test_report_process, dependent: :destroy

  private

  def create_test_report_status
    TestReportProcess.create test_report: self, status: :created
  end
end
