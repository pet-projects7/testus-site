# frozen_string_literal: true

class Scenario < ApplicationRecord
  include AvailableStatuses
  include Taggable

  belongs_to :feature
  has_many :steps, dependent: :destroy
end
