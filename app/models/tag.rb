# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :taggings

  has_many :features, through: :taggings, source: :taggable,
                      source_type: "Feature"
  has_many :features, through: :taggings, source: :taggable,
                      source_type: "Scenario"
end
