# frozen_string_literal: true

require "dry/schema"

module Report
  FeatureSchema = Dry::Schema.Params do
    required(:uri).filled(:string)
    required(:id).filled(:string)
    required(:keyword).maybe(:string)
    required(:name).maybe(:string)
    required(:description).maybe(:string)
    # Сценарии
    required(:elements).array(ScenarioSchema)
  end
end
