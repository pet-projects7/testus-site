# frozen_string_literal: true

require "dry/schema"

module Report
  ResultSchema = Dry::Schema.Params do
    required(:status).maybe(:string)
    optional(:duration).filled(:integer)
  end
end
