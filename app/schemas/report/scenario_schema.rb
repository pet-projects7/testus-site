# frozen_string_literal: true

require "dry/schema"

module Report
  ScenarioSchema = Dry::Schema.Params do
    required(:name).maybe(:string)
    required(:description).maybe(:string)
    required(:line).maybe(:integer)
    required(:type).maybe(:string)
    optional(:tags).array(TagSchema)

    optional(:after).array(:hash) do
      required(:match).maybe(:hash) do
        required(:location).maybe(:string)
      end
      required(:result).maybe(ResultSchema)
    end
    required(:steps).array(StepSchema)
  end
end
