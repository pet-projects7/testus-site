# frozen_string_literal: true

require "dry/schema"

module Report
  StepSchema = Dry::Schema.Params do
    required(:keyword).maybe(:string)
    required(:name).maybe(:string)
    required(:line).maybe(:integer)
    required(:match).maybe(:hash) do
      required(:location).maybe(:string)
    end
    required(:result).maybe(ResultSchema)
  end
end
