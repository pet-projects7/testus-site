# frozen_string_literal: true

require "dry/schema"

module Report
  TagSchema = Dry::Schema.Params do
    required(:name).maybe(:string)
    required(:line).maybe(:integer)
  end
end
