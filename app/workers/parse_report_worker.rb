# frozen_string_literal: true

class ParseReportWorker
  include Sidekiq::Worker

  def perform(test_report_id)
    test_report = TestReport.find(test_report_id)
    json_with_features = features(test_report)
    command_response = ParseReportCommand.call(json_with_features)

    return unless command_response.success?

    parsed_features = command_response.result[:features]

    parsed_features.each do |feature|
      SaveReportToDbCommand.call(feature, test_report)
    end

    test_report.update(status: command_response.result[:test_report][:status])
    test_report.update(tags: fetch_test_report_tags(command_response))

    test_report.test_report_process.update(status: :processed_with_success)

    notify_users_about_failures(test_report) if command_response.result[:test_report][:status].include? "failed"
  end

  private

  def notify_users_about_failures(test_report)
    failed_examples = test_report.features.with_status(:failed).pluck(:name)
    project = test_report.project

    project.users_to_notify.each do |user_setting|
      email_params = {
        email: user_setting.user.email,
        failed_examples: failed_examples,
        project_name: project.title,
        project: project,
        test_report: test_report
      }

      ReportFailuresAlertMailer.with(email_params).notify.deliver_now
    end
  end

  def features(test_report)
    file_path = ActiveStorage::Blob.service.path_for(test_report.report_file.key)

    file = File.open file_path
    # rubocop:disable Security/JSONLoad:
    JSON.load file
    # rubocop:enable Security/JSONLoad:
  end

  def fetch_test_report_tags(command_response)
    command_response
      .result[:test_report][:tags]
      .map { |tag_name| Tag.find_or_create_by(title: tag_name) }
  end
end
