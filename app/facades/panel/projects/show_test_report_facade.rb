# frozen_string_literal: true

module Panel
  module Projects
    class ShowTestReportFacade < DocumentationFacade
      attr_reader :test_report

      def initialize(project, test_report)
        super(project, {})
        @test_report = test_report
      end
    end
  end
end
