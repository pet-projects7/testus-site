# frozen_string_literal: true

module Panel
  module Projects
    class DocumentationFacade
      attr_reader :project

      def initialize(project, params)
        @project = project
        @params = params
      end

      def test_report
        @test_report ||= @project.test_reports.last
      end

      def features
        @features ||= Panel::FeatureQueryObject.new(test_report.features).search(@params[:filter] || {})
      end

      def last_check_ago
        ActionController::Base.helpers.time_ago_in_words(test_report.created_at)
      end

      def finished_scenarios_count
        test_report.scenarios.count
      end

      def failures_count
        test_report.features.with_status(:failed).count
      end

      def status_of_test_report
        return nil if test_report.nil?

        test_report.test_report_process.status || nil
      end

      def ready_to_read
        status_of_test_report == "processed_with_success"
      end
    end
  end
end
