class BaseController {
  body: HTMLElement

  bodyControllerName: string

  controllerName: string

  constructor(controllerName: string) {
    this.body = document.querySelector('body')!
    this.bodyControllerName = this.body.dataset.controller!

    this.controllerName = controllerName

    if(this.controllerName == this.bodyControllerName) {
      this.init()
    }
  }

  init () {
    throw 'This method must be reimplemented'
  }
}

export { BaseController }
