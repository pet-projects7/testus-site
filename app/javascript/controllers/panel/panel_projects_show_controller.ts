import Toggler from '@/components/toggler'
import {TestReportViewer} from '@/components/panel/testReportViewer';
import Choices from 'choices.js'
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';

class PanelProjectsShowController {
  testReportViewer: TestReportViewer
  scenariosContainer: HTMLElement

  constructor () {
    this.base()
    this.initReportViewer()

    const choices = new Choices('[name="filter[status]"]', {
      searchEnabled: false,
      itemSelectText: '',
    });

    choices.passedElement.element.addEventListener('change', (event: any) => {
      this.changeUrl('filter[status]', event.detail.value)
    })

    const tags = new Choices('[name="filter[tags]"]', {
      searchEnabled: false,
      itemSelectText: '',
    });

    tags.passedElement.element.addEventListener('change', (event: any) => {
      this.changeUrl('filter[tags]', event.detail.value)
    })
    // tippy('[data-tippy-content]');
  }

  initReportViewer() {
    this.scenariosContainer =  document.querySelector("[data-selector='scenariosList']") as HTMLElement
    this.testReportViewer = new TestReportViewer(this.scenariosContainer)
  }

  base() {
    // eslint-disable-next-line no-new
    new Toggler({
      selector: '[data-module-collapsed]'
    })
  }

  changeUrl(filterName: string, value: string) {
    const currentPath = window.location.href

    let parsedURL = new URL(currentPath)

    if (value) {
      parsedURL.searchParams.set(filterName, value)
    } else {
      parsedURL.searchParams.delete(filterName)
    }

    window.location.href = parsedURL.href
  }
}

export { PanelProjectsShowController }
