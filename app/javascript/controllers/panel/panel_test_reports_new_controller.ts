class PanelTestReportsNewController {
  constructor() {
    this.setFormHandler()
  }

  setFormHandler() {
    const form = document.querySelector("[data-selector='form']")

    if (form) {
      form.addEventListener('ajax:success', (event: any ) => {
        const result = event.detail[0]

        window.location = result.data.reportPath
      })
    }
  }
}

export { PanelTestReportsNewController }
