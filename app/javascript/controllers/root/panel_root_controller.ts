import {PanelProjectsShowController} from '@/controllers/panel/panel_projects_show_controller';
import {PanelTestReportsNewController} from "controllers/panel/panel_test_reports_new_controller";
import {DemoProjectsIndexController} from "controllers/demo_prtojects/demo_projects_index_controller";

class PanelRootController {
  pageControllerName: string

  constructor() {
    const body = document.querySelector('body')!
    const bodyControllerName = body.dataset.controller!

    this.pageControllerName = bodyControllerName
  }

  defined_controllers() {
    return [
      // /panel/projects/:project_id Страница просмотра проекта
      {
        baseSelector: ['panel_projects_show', 'public_projects_show', 'panel_test_reports_show', 'demo_projects_show'],
        controller: PanelProjectsShowController
      },

      // /panel/projects/:project_id/test_reports/new Ручное добавление репортов
      {
        baseSelector: ['panel_test_reports_new'],
        controller: PanelTestReportsNewController
      },

      // /demo_projects Добавление демо-проекта
      {
        baseSelector: ['demo_projects_index'],
        controller: DemoProjectsIndexController
      },
    ]
  }

  selectController() {
    var controllerForPage;

    this.defined_controllers().forEach((controllerPair) => {
      if (controllerPair.baseSelector.indexOf(this.pageControllerName) >= 0) {
        controllerForPage = controllerPair.controller
      }
    })

    return controllerForPage || Function
  }
}

export {PanelRootController}
