interface IProfileMenu {
  selector: string
}

class ProfileMenu {
  private options: IProfileMenu;

  constructor (options: IProfileMenu) {
    this.options = options
    document.addEventListener('click', this.toggle.bind(this))
  }

  toggle (event: Event): void {
    const collapsedContainer = (event.target as Element).closest(this.options.selector)

    if (collapsedContainer) {
      collapsedContainer.classList.toggle('collapsed')
    }
  }
}

export default ProfileMenu
