import axios from 'axios'

class TestReportViewer {
  scenariosContainer: HTMLElement

  activeMenuItemCSSClass = 'leftMenu__item--active'

  ui = {
    menuContainer: document.querySelector('[data-selector=\'testReport__features\']')!,
    menuItems: document.querySelectorAll('[data-feature-id]')!
  }

  constructor(scenariosContainer: HTMLElement) {
    this.scenariosContainer = scenariosContainer
    this.setClickMenuItemHandler()
    this.loadFirstOrSelectedFeature()
  }

  loadFirstOrSelectedFeature() {
    const firstFeatureID = this.ui.menuItems && (this.ui.menuItems[0] as HTMLElement).dataset['featureId'] as string

    let featureParamInUrl = window.location.search.match(/feature=(\d+)$/)
    let selectedFeature = featureParamInUrl && featureParamInUrl[1]
    selectedFeature = selectedFeature || firstFeatureID

    if(selectedFeature) {
      const menuItem = this.ui.menuContainer.querySelector(`[data-feature-id="${ selectedFeature }"]`) as HTMLElement

      this.setActiveMenuItem(menuItem)
      this.selectFeature(selectedFeature)
    }
  }

  setClickMenuItemHandler() {
    this.ui.menuContainer
      .addEventListener('click', (event) => {
        const element = event.target as HTMLElement
        const featureID = element.dataset.featureId

        if (featureID) {
          this.changeUrl(element, featureID)
          this.selectFeature(featureID)
          this.setActiveMenuItem(element)
        }
      })
  }

  selectFeature(featureID: string) {
    const urlParams = new URLSearchParams(window.location.search)
    const requestParams = {
      featureID,
      status: urlParams.get("filter[status]"),
      tags: urlParams.get("filter[tags]")
    }

    axios
      .get(`/panel/features/${ featureID }/scenarios`, { params: requestParams})
      .then((response) => {
        this.scenariosContainer.innerHTML  = response.data
      })
  }

  changeUrl(element: HTMLElement, featureID: string) {
    const currentPath = window.location.pathname.replace(/\/$/, '')

    window.history.replaceState({}, element.innerText, `${ currentPath }/?feature=${ featureID }`);
  }

  setActiveMenuItem(element: HTMLElement) {
    this.ui
      .menuItems
      .forEach(menuItem => menuItem.classList.toggle(this.activeMenuItemCSSClass, false))

    element.classList.toggle(this.activeMenuItemCSSClass, true)
  }
}

export {TestReportViewer}
