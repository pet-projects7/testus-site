import {PanelRootController} from "@/controllers/root/panel_root_controller";
import '../src/panel.scss'


require('@rails/ujs').start()
require('@rails/activestorage').start()
require('channels')

document.addEventListener('DOMContentLoaded', () => {
  const panelRootController = new PanelRootController()
  const selectedController = panelRootController.selectController()

  new selectedController
})

require.context('../images', true)
