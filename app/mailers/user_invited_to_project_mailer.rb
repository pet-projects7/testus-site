# frozen_string_literal: true

class UserInvitedToProjectMailer < ApplicationMailer
  def invite
    @user = params[:user]
    @project = params[:project]

    mail(to: @user.email, subject: "Invitation to mnogotestov.ru")
  end
end
