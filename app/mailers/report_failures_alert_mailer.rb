# frozen_string_literal: true

class ReportFailuresAlertMailer < ApplicationMailer
  def notify
    @email = params[:email]
    @failed_examples = params[:failed_examples]
    @project_name = params[:project_name]
    @project = params[:project]
    @test_report = params[:test_report]

    @link_to_test_report = panel_project_test_report_url(project_id: @project.id,
                                                         id: @test_report.id,
                                                         filter: { status: :failed })

    mail(to: @email, subject: "Failed  features on #{@project_name}")
  end
end
