# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: "hi@mnogotestov.ru"
  layout "mailer"
end
