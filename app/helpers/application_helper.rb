# frozen_string_literal: true

module ApplicationHelper
  def uniq_css_class
    "#{controller_path.tr '/', '_'}_#{action_name}"
  end
end
