
# README

Create cucumber report
```
bundle exec cucumber --format json --out report.json --format pretty
```

Updating CI image on Gitlab CI

```
docker build . -t registry.gitlab.com/pet-projects7/testus-site:latest
docker login registry.gitlab.com -u <username> -p <password>
docker push registry.gitlab.com/pet-projects7/testus-site:latest
```
