docker_local:
	docker-compose -f ./docker/docker-compose.yml up

docker_run_command:
	docker-compose run

setup_server:
	ansible-playbook ansible/playbooks/setup.yml -i ansible/hosts

deploy:
	BRANCH=$(BRANCH) ansible-playbook ansible/playbooks/deploy.yml -i ansible/hosts
