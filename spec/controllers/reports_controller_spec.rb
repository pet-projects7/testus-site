# frozen_string_literal: true

RSpec.describe ReportsController, type: :request do
  # let!(:file) do
  #   File.open 'spec/fixtures/elk.json'
  # end
  let(:file) { fixture_file_upload("spec/fixtures/files/elk.json") }
  let!(:project) { create(:project) }

  describe "#new" do
    it "creates report in database" do
      params = { report: file, token: project.token }

      expect do
        post reports_path, params: params
      end.to change(TestReport, :count).by(1)
    end

    # it 'responds with not found error when token key invalid' do
    #   params = { report: file, token: 'token' }
    #
    #   post reports_path, params: params
    #   response_body = JSON.parse(response.body)
    #
    #   expect(response_body).to eq('error' => 'Project not found')
    # end
  end
end
