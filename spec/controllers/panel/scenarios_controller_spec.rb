# frozen_string_literal: true

require "rails_helper"

RSpec.describe Panel::ScenariosController, type: :request do
  let(:user) { create(:user) }
  let(:file) { fixture_file_upload(file_fixture("etp.json")) }
  let!(:project) { create(:project, users: [user]) }

  before do
    user.confirm
    sign_in user
    upload_test_report project
  end

  describe "#index" do
    it "renders list of scenarios without any parameters" do
      feature_id = TestReport.last.features.with_status(:failed).first.id

      get panel_feature_scenarios_path(feature_id)
      expect(response.body).to match(/Когда  Кликается по «Все документы» ГК Газпром/)
    end
  end

  def upload_test_report(project)
    params = {
      token: project.token,
      report: file
    }

    post reports_path, params: params
    ParseReportWorker.new.perform(TestReport.last.id)
  end
end
