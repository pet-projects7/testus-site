# frozen_string_literal: true

require "rails_helper"

RSpec.describe Panel::ProjectsController, type: :request do
  let(:user) { create(:user, name: "Maks") }
  let!(:project) { create(:project, users: [user]) }

  before do
    user.confirm
    sign_in user
  end

  describe "#index" do
    it "renders list of projects" do
      get panel_projects_path
      expect(@response.body).to match(project.title.to_s)
    end
  end
end
