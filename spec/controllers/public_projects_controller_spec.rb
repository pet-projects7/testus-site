# frozen_string_literal: true

RSpec.describe PublicProjectsController, type: :request do
  describe "#index" do
    it "renders list of public projects" do
    end
  end

  describe "#show" do
    it "renders a test report" do
    end

    it "renders the error message" do
    end
  end
end
