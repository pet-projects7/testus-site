# frozen_string_literal: true

RSpec.describe DemoProjectsController, type: :request do
  let(:project) { create(:project) }
  let(:demo_project) { create(:project, :demo) }
  let(:test_report) { create(:test_report, :demo) }
  let(:file) { fixture_file_upload(file_fixture("etp.json")) }

  describe "#index" do
    it "renders correct template" do
      get demo_projects_path
      expect(response).to render_template(:index)
    end
  end

  describe "#show" do
    it "renders restrict message" do
      get demo_project_path(project)
      expect(response.body).to match(/Sorry, or we don't have such project or this project is not public/)
    end

    it "renders a test report of selected project" do
      send_report
      ParseReportWorker.new.perform(TestReport.last.id)

      get demo_project_path(DemoProject.last)
      expect(response.body).to match(/Пользователь зашел на страницу «Банковское сопровождение»/)
    end
  end

  describe "#create" do
    it "saves sended test report" do
      send_report
    end
  end

  private

  def send_report
    post_params = {
      demo_project: {
        title: "Project name",
        test_reports_attributes: {
          "0": { report_file: file }
        }
      }
    }

    post demo_projects_path, params: post_params
  end
end
