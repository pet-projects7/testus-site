# frozen_string_literal: true

RSpec.describe ReportsController, type: :request do
  let!(:page_name) { "instructions" }

  describe "#show" do
    it "renders particular page" do
      page_response = get page_path(id: page_name)
      expect(page_response).to eq(200)
    end

    it "renders 404 when page is not find" do
      expect { get page_path(id: "page_name") }.to raise_error(ActionController::RoutingError)
    end
  end
end
