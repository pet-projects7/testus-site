# frozen_string_literal: true

require "rails_helper"

RSpec.describe Step, type: :model do
  it { is_expected.to define_enum_for(:cucumber_type) }
  it { is_expected.to belong_to(:scenario) }
end
