# frozen_string_literal: true

require "rails_helper"

RSpec.describe Feature, type: :model do
  it { is_expected.to belong_to(:test_report) }
  it { is_expected.to have_many(:scenarios).dependent(:destroy) }
  it { is_expected.to have_many(:tags) }
end
