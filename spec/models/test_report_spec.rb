# frozen_string_literal: true

require "rails_helper"

RSpec.describe TestReport, type: :model do
  let(:test_report) { create(:test_report) }

  it { is_expected.to belong_to(:project) }
  it { is_expected.to have_many(:features).dependent(:destroy) }

  it "adds test report process model after creating" do
    expect(test_report.test_report_process.status).to eq("created")
  end
end
