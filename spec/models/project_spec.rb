# frozen_string_literal: true

require "rails_helper"

RSpec.describe Project, type: :model do
  let(:project) { create(:project) }

  it { is_expected.to have_many(:test_reports).dependent(:destroy) }
  it { is_expected.to have_and_belong_to_many(:users) }
  it { is_expected.to validate_presence_of(:title) }

  it "has a token" do
    expect(project.token).not_to be_empty
  end
end
