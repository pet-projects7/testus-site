# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  it { is_expected.to have_and_belong_to_many(:projects) }
  it { is_expected.to have_many(:test_reports) }
  it { is_expected.to validate_presence_of(:name) }
end
