# frozen_string_literal: true

module Components
  module Application
    module SignIn
      class Form
        include RSpec::Matchers
        include Capybara::DSL

        def fill(user)
          fill_in "Email", with: user.email
          fill_in "Password", with: user.password

          click_on "Log in"
        end
      end
    end
  end
end
