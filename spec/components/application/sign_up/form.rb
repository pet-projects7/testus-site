# frozen_string_literal: true

module Components
  module Application
    module SignUp
      class Form
        include RSpec::Matchers
        include Capybara::DSL

        def fill
          fill_in "Name", with: "Maksim"
          fill_in "Email", with: "client@mail.ru"
          fill_in "Password", with: "password"
          fill_in "Password confirmation", with: "password"

          click_on "Submit"
        end
      end
    end
  end
end
