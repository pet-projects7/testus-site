# frozen_string_literal: true

require "rails_helper"
require "pages/application/main_page"
require "components/application/sign_in/form"

RSpec.describe "Sign In user", type: :feature do
  let(:main_page) { ::Pages::Application::MainPage.new }
  let(:sign_in_form) { ::Components::Application::SignIn::Form.new }
  let(:user) { create(:user, :confirmed) }

  it "will allow signing in with correct credentials" do
    main_page.visit_page
    find(".application-header__panelLink").click
    sign_in_form.fill(user)

    expect(page).to have_content("Мои проекты")
  end
end
