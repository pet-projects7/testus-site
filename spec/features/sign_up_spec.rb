# frozen_string_literal: true

require "rails_helper"
require "pages/application/main_page"
require "components/application/sign_up/form"

RSpec.describe "Sign Up user", type: :feature do
  let(:main_page) { ::Pages::Application::MainPage.new }
  let(:sign_up_form) { ::Components::Application::SignUp::Form.new }

  it "sign up requires confirmation" do
    expect do
      main_page.visit_page
      find(".application-header__panelLink").click
      click_on "Sign up"

      sign_up_form.fill
    end.to change { ActionMailer::Base.deliveries.size }.by(1)

    ActionMailer::Base.deliveries.last.body.match(/href="(.+)"/)[1]
  end
end
