# frozen_string_literal: true

# https://github.com/teamcapybara/capybara

require "rails_helper"
require "pages/application/main_page"
require "components/application/sign_in/form"

RSpec.describe "Project creating", type: :feature do
  let(:user) { create(:user, :confirmed) }

  before do
    login_as(user, scope: :user)
  end

  it "creates a project" do
    visit panel_home_path

    expect(page).to have_content("Мои проекты")

    click_on "Добавить проект"
    fill_in "Название проекта", with: "Google"
    fill_in "Краткое описание проекта", with: "Google"
    click_on "Создать проект"

    expect(page).to have_content("Google")
  end
end
