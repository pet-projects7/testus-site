# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { "Bill Gates" }
    email { "user@user.ru" }
    password { "1xP5dRT7" }

    trait :confirmed do
      before(:create, &:skip_confirmation!)
    end
  end
end
