# frozen_string_literal: true

FactoryBot.define do
  factory :test_report do
    duration { 110 }
    project
    result { "passed" }

    trait :passed do
      result { "passed" }
    end
  end
end
