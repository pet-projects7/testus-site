# frozen_string_literal: true

FactoryBot.define do
  factory :scenario do
    cucumber_id { 'страница-личные-данные;переход-на-страницу-"личные-данные"-из-меню-пользователя;пользователи;2' }
    keyword { "Структура сценария" }
    name { 'Переход на страницу "Личные данные" из меню пользователя' }
    description { "" }
    line { 20 }
    type { "scenario" }
    feature
  end
end
