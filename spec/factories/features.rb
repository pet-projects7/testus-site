# frozen_string_literal: true

FactoryBot.define do
  factory :feature do
    uri { "features/views/Home/UserNavigation.feature" }
    cucumber_id { "страница-личные-данные" }
    keyword { "Функционал" }
    name { "Страница Личные данные" }
    description { "" }
    test_report
  end
end
