# frozen_string_literal: true

FactoryBot.define do
  factory :user_project_setting do
    user
    project
    name { :notify_on_failures }
  end
end
