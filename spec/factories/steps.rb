# frozen_string_literal: true

FactoryBot.define do
  factory :step do
    step_type { "test" }
    keyword { "Допустим " }
    name { 'пользователь авторизовался "nimluzospa@desoz.com" и "Test1234"' }
    line { 12 }
    location { "features/step_definitions/views/Home/UserNavigation.rb:29" }
    status { "passed" }
    duration { 251_379_362 }
    scenario

    trait :background_type do
      type { "background" }
    end
  end
end
