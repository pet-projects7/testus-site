# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    title { "Yandex" }
    description { "https://yandex.ru" }

    trait(:demo) do
      type { DemoProject }
    end
  end
end
