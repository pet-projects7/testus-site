# frozen_string_literal: true

RSpec.describe SaveReportToDbCommand do
  context "when ELK Report" do
    let!(:file) do
      file = File.open "spec/fixtures/files/elk.json"
      # rubocop:disable Security/JSONLoad
      JSON.load file
      # rubocop:enable Security/JSONLoad
    end
    let!(:project) { create(:project) }
    let!(:test_report) { create(:test_report) }

    it "saves scenarios report in database" do
      report = ParseReportCommand.call(file).result
      feature = report[:features].first

      expect do
        described_class.call(feature, test_report)
      end.to change(Scenario, :count).from(0).to(7)
    end

    it "saves steps report in database" do
      report = ParseReportCommand.call(file).result
      feature = report[:features].first

      expect do
        described_class.call(feature, test_report)
      end.to change(Step, :count).from(0).to(53)
    end
  end
end
