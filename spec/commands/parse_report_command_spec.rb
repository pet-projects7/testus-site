# frozen_string_literal: true

require "json"
require "fileutils"

RSpec.describe ParseReportCommand do
  context "when report valid" do
    let(:reports) do
      [
        ["spec/fixtures/files/theway.json", 4],
        ["spec/fixtures/files/reek.json", 29],
        ["spec/fixtures/files/alians.json", 2],
        ["spec/fixtures/files/elk.json", 3],
        ["spec/fixtures/files/arbitrcourt.json", 1],
        ["spec/fixtures/files/react-admin.json", 11]
      ]
    end

    it "returns features" do
      reports.each do |report|
        file = load_file(report[0])
        features = described_class.call(file).result[:features]
        expect(features.count).to eq report[1]
      end
    end

    it "concatenates scenarios from same features into one feature" do
      file = load_file("spec/fixtures/files/etp.json")
      features = described_class.call(file).result[:features]
      expect(features.count).to eq(33)
    end
  end

  # context 'invalid report' do
  #   let(:invalid_report) { 'spec/fixtures/invalid_report.json' }
  #
  #   it 'returns errors when report schema is invalid' do
  #     file = load_file(invalid_report)
  #     features = described_class.call(file)
  #     expect(features.success?).to be(false)
  #     expect(features.errors[:report].first).to eq('Schema of report is invalid')
  #   end
  # end

  private

  def load_file(path)
    file = File.open path
    # rubocop:disable Security/JSONLoad
    JSON.load file
    # rubocop:enable Security/JSONLoad
  end
end
