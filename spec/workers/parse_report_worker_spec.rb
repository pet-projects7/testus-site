# frozen_string_literal: true

require "rails_helper"

RSpec.describe ParseReportWorker, type: :worker do
  let(:user) { create(:user) }
  let(:project) { create(:project, users: [user]) }
  let(:test_report) { create(:test_report, project: project) }
  let!(:notify_on_failures_setting) { create(:user_project_setting, user: user, project: project) }
  let(:report_files_path) do
    [
      "spec/fixtures/files/elk.json",
      "spec/fixtures/files/arbitrcourt.json",
      "spec/fixtures/files/etp.json",
      "spec/fixtures/files/reek.json",
      "spec/fixtures/files/theway.json",
      "spec/fixtures/files/alians.json"
    ]
  end

  it "handles report correctly" do
    report_files_path.each do |file_path|
      file_name = file_path.split("/").last
      test_report.report_file.attach(io: File.open(file_path), filename: file_name)

      expect { described_class.new.perform(test_report.id) }.not_to raise_error
    end
  end

  it "notifies user when report has failed features" do
    test_report.report_file.attach(io: File.open("spec/fixtures/files/etp.json"),
                                   filename: "etp.json")

    expect { described_class.new.perform(test_report.id) }.not_to raise_error
  end
end
