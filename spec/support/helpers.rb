# frozen_string_literal: true

module Helpers
  def self.load_file(path)
    file = File.open path
    # rubocop:disable Security/JSONLoad
    JSON.load file
    # rubocop:enable Security/JSONLoad
  end
end
