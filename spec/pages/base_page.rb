# frozen_string_literal: true

module Pages
  class BasePage
    include Capybara::DSL # Capybara
    include FactoryBot::Syntax::Methods # FactoryBot
    include Warden::Test::Helpers # Devise
    include Rails.application.routes.url_helpers # Routes
  end
end
