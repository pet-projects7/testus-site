# frozen_string_literal: true

require "pages/base_page"

module Pages
  module Application
    class MainPage < BasePage
      def visit_page
        visit root_path
        self
      end
    end
  end
end
