# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/report_failures_alert
class ReportFailuresAlertPreview < ActionMailer::Preview
  def notify
    test_report = TestReport.last
    project = test_report.project
    failed_features = TestReport.last.features.with_status("failed").pluck(:name)

    preview_params = {
      email: "1@1.ru",
      project_name: "Porject Name",
      failed_examples: failed_features,
      project: project,
      test_report: test_report
    }

    ReportFailuresAlertMailer.with(preview_params).notify
  end
end
