# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/user_invited_to_project
class UserInvitedToProjectPreview < ActionMailer::Preview
end
