# frozen_string_literal: true

require "rails_helper"

RSpec.describe UserInvitedToProjectMailer, type: :mailer do
  let(:user) { create(:user, name: "Maksim") }
  let(:project) { create(:project) }

  describe "invite" do
    it "shoould render a email letter" do
      email = described_class.with(project: project, user: user).invite.deliver

      expect(email.body).to have_content("You have been added to project Yandex")
    end
  end
end
