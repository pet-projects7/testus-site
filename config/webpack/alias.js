const path = require('path')

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '..', '..', 'app/javascript'),
      '@views': path.resolve(__dirname, '..', '..', 'app/javascript/views'),
      '@components': path.resolve(__dirname, '..', '..', 'app/javascript/components')
    }
  }
}
