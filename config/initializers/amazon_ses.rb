# frozen_string_literal: true

# unless Rails.env.test?
#   access_key_id = Rails.application.credentials.ses[:ACCESS_KEY]
#   secret_access_key = Rails.application.credentials.ses[:SECRET_KEY]

#   ActionMailer::Base.add_delivery_method :ses, AWS::SES::Base,
#                                          access_key_id: access_key_id,
#                                          secret_access_key: secret_access_key,
#                                          signature_version: 4,
#                                          region: 'eu-central-1'
# end

if Rails.env.production?
  access_key_id = Rails.application.credentials.ses[:ACCESS_KEY]
  secret_access_key = Rails.application.credentials.ses[:SECRET_KEY]

  Aws::Rails.add_action_mailer_delivery_method(
    :ses,
    credentials: Aws::Credentials.new(
      access_key_id,
      secret_access_key
    ),
    region: "eu-central-1"
  )
end
