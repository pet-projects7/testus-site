# frozen_string_literal: true

redis_host = ENV["REDIS_HOST"] || "127.0.0.1"
redis_url = "redis://#{redis_host}:6379/0"

Sidekiq.configure_server do |config|
  config.redis = { url: redis_url }
end

Sidekiq.configure_client do |config|
  config.redis = { url: redis_url }
end
