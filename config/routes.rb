# frozen_string_literal: true

require "sidekiq/web"

Rails.application.routes.draw do
  devise_for :users, controllers: { confirmations: "users/confirmations" }
  resource :reports # Основной роут для отправки репортов по api
  root to: "home#index"

  post "select_locale", to: "application#select_locale"

  scope :panel, module: :panel, as: :panel do
    get "/", to: "projects#index", as: :home
    get "/profile", to: "users#profile", as: :profile

    resources :projects do
      get "settings", as: :settings
      get "history", as: :history
      resources :members, only: %i[index create]

      resources :test_reports, only: %i[new show] do
        get "status", as: :status
        resources :features, only: [], shallow: true do
          resources :scenarios, only: :index
        end
      end
    end

    # get '/', to: 'panel#home', as: :home
    get "/lists", to: "panel#lists", as: :lists
  end

  resources :public_projects, only: %i[index show]
  resources :demo_projects, only: %i[index new show create]
  resources :pages, only: [:show], constraints: lambda { |params|
    %w[instructions about].include?(params[:id])
  }
  # get '/public/projects/:project_id', to: 'public_projects#show'
  mount Sidekiq::Web => "/sidekiq"
end
