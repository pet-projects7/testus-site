# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock "~> 3.13"

set :application, "testus"
set :repo_url, "git@gitlab.com:testus-group/testus-site.git"
set :branch, ENV["BRANCH"] || "master"
set :deploy_to, "/var/www/testus"
set :rvm_ruby_version, "2.7.4"
set :nvm_node, "v13.11.0"
set :nvm_map_bins, %w[node npm yarn]

append :linked_files, "config/master.key"
append :linked_dirs,
       "log",
       "tmp/pids",
       "tmp/cache",
       "tmp/sockets",
       "public/system",
       "node_modules",
       "public/sitemaps"

# set :format, :pretty
set :log_level, :info

set :keep_releases, 5

set :db_local_clean, true
set :db_remote_clean, true

set :default_env, {
  "PATH" => "$HOME/.nvm/versions/node/v13.11.0/bin:$PATH"
}

set :nginx_server_name, "mnogotestov.ru"

namespace :puma do
  Rake::Task[:restart].clear_actions

  desc "Overwritten puma:restart task"
  task :restart do
    # rubocop:disable Rails/Output
    puts "Overwriting puma:restart to ensure that puma is running. Effectively, we are just starting Puma."
    puts "A solution to this should be found."
    # rubocop:enable Rails/Output
    invoke "puma:stop"
    invoke "puma:start"
  end
end
