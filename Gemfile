# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.7.4"

gem "jbuilder", "~> 2.7"
gem "pg", ">= 0.18", "< 2.0"
gem "puma", "~> 3"
gem "rails", "~> 6.0.0"
gem 'sassc-rails'
gem "webpacker", "~> 4.0"
# Use Active Storage variant
# gem 'image_processing', '~> 1.2'
gem "array_enum"
gem "aws-sdk-rails"
gem "babosa"
gem "bootsnap", ">= 1.4.2", require: false
gem "devise"
gem "dry-schema"
gem "friendly_id"
gem "rails-i18n", "~> 6.0.0" # For 6.0.0 or higher
gem "rollbar"
gem "scss_lint", require: false
gem "sidekiq", "~> 5"
gem "simplecov", require: false, group: :test
gem "slim"
gem "trailblazer"
gem "trailblazer-cells"
gem "trailblazer-rails" # if you are in rails.

group :development, :test do
  gem "byebug", platforms: %i[mri mingw x64_mingw]
  gem "lefthook"
  gem "rspec-rails", "~> 3.8"
  gem "rubocop"
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rspec", require: false
  gem "standard"

  # Деплой
  gem "capistrano", "~> 3.10", require: false
  gem "capistrano3-nginx"
  gem "capistrano3-puma"
  gem "capistrano-db-tasks", require: false
  gem "capistrano-rails", "~> 1.3", require: false
  gem "capistrano-rvm"
  gem "capistrano-sidekiq"
end

group :development do
  gem "letter_opener"
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "web-console", ">= 3.3.0"
end

group :test do
  gem "brakeman"
  gem "bundler-audit"
  gem "capybara", ">= 2.15"
  gem "database_cleaner"
  gem "factory_bot_rails"
  gem "rails-controller-testing"
  gem "selenium-webdriver"
  gem "shoulda-matchers"
  gem "webdrivers"
end

gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]
