# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_19_174755) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "features", force: :cascade do |t|
    t.string "uri"
    t.string "cucumber_id"
    t.string "keyword"
    t.string "name"
    t.text "description"
    t.bigint "test_report_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: [], array: true
    t.string "duration"
    t.string "slug"
    t.index ["test_report_id"], name: "index_features_on_test_report_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "token"
    t.boolean "public", default: false
    t.string "slug"
    t.string "type"
  end

  create_table "projects_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "project_id", null: false
    t.integer "role"
    t.boolean "should_notify"
  end

  create_table "scenarios", force: :cascade do |t|
    t.string "cucumber_id"
    t.string "keyword"
    t.string "name"
    t.string "description"
    t.string "line"
    t.string "cucumber_type"
    t.bigint "feature_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: [], array: true
    t.string "duration"
    t.index ["feature_id"], name: "index_scenarios_on_feature_id"
  end

  create_table "steps", force: :cascade do |t|
    t.integer "cucumber_type"
    t.string "keyword"
    t.string "name"
    t.integer "line"
    t.string "location"
    t.integer "status", default: [], array: true
    t.string "duration"
    t.bigint "scenario_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["scenario_id"], name: "index_steps_on_scenario_id"
  end

  create_table "tagging", force: :cascade do |t|
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.bigint "tag_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tag_id"], name: "index_tagging_on_tag_id"
    t.index ["taggable_type", "taggable_id"], name: "index_tagging_on_taggable_type_and_taggable_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["title"], name: "index_tags_on_title", unique: true
  end

  create_table "test_report_processes", force: :cascade do |t|
    t.bigint "test_report_id", null: false
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "errors_messages", default: [], array: true
    t.index ["test_report_id"], name: "index_test_report_processes_on_test_report_id"
  end

  create_table "test_reports", force: :cascade do |t|
    t.string "duration"
    t.bigint "project_id", null: false
    t.integer "result"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: [], array: true
    t.integer "source"
    t.index ["project_id"], name: "index_test_reports_on_project_id"
  end

  create_table "user_project_settings", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "project_id", null: false
    t.bigint "user_id", null: false
    t.integer "name", null: false
    t.index ["name", "project_id", "user_id"], name: "index_user_project_settings_on_name_and_project_id_and_user_id", unique: true
    t.index ["project_id"], name: "index_user_project_settings_on_project_id"
    t.index ["user_id"], name: "index_user_project_settings_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "name"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "features", "test_reports"
  add_foreign_key "scenarios", "features"
  add_foreign_key "steps", "scenarios"
  add_foreign_key "test_report_processes", "test_reports"
  add_foreign_key "test_reports", "projects"
  add_foreign_key "user_project_settings", "projects"
  add_foreign_key "user_project_settings", "users"
end
