# frozen_string_literal: true

class AddSlugToPost < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :slug, :string
    add_column :features, :slug, :string
  end
end
