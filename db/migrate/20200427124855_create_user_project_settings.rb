# frozen_string_literal: true

class CreateUserProjectSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :user_project_settings do |t|
      t.timestamps

      t.belongs_to :project, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true
      t.integer :name, null: false
    end
  end
end
