# frozen_string_literal: true

class DeleteTagsJoinTables < ActiveRecord::Migration[6.0]
  def change
    drop_join_table :scenarios, :tags if table_exists?(:scenarios_tags)
    drop_join_table :features, :tags if table_exists?(:features_tags)
    drop_join_table :test_reports, :tags if table_exists?(:tags_test_reports)
  end
end
