# frozen_string_literal: true

class CreateFeatures < ActiveRecord::Migration[6.0]
  def change
    create_table :features do |t|
      t.string :uri
      t.string :cucumber_id
      t.string :keyword
      t.string :name
      t.text :description
      t.belongs_to :test_report, null: false, foreign_key: true

      t.timestamps
    end
  end
end
