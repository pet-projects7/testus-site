# frozen_string_literal: true

class AddRoleNotifyFields < ActiveRecord::Migration[6.0]
  def change
    add_column :projects_users, :role, :integer
    add_column :projects_users, :should_notify, :boolean
  end
end
