# frozen_string_literal: true

class ChamgeStatusFromIntegerToArray < ActiveRecord::Migration[6.0]
  def change
    tables = %i[scenarios features test_reports steps]

    tables.each do |table_name|
      change_column table_name,
                    :status,
                    :integer,
                    array: true,
                    default: [],
                    using: "array[status]::INTEGER[]"
    end
  end
end
