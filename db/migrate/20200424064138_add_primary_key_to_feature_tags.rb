# frozen_string_literal: true

class AddPrimaryKeyToFeatureTags < ActiveRecord::Migration[6.0]
  def change
    add_column :features_tags, :id, :primary_key
  end
end
