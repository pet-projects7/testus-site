# frozen_string_literal: true

class AddProjectUserJoin < ActiveRecord::Migration[6.0]
  def change
    create_join_table :users, :projects
  end
end
