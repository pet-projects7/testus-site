# frozen_string_literal: true

class AddErrorsToProcess < ActiveRecord::Migration[6.0]
  def change
    add_column :test_report_processes,
               :errors_messages,
               :string,
               array: true,
               default: [],
               using: "array[status]::STRING[]"
  end
end
