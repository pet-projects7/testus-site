# frozen_string_literal: true

class CreateTestReports < ActiveRecord::Migration[6.0]
  def change
    create_table :test_reports do |t|
      t.integer :duration
      t.belongs_to :project, null: false, foreign_key: true
      t.integer :result

      t.timestamps
    end
  end
end
