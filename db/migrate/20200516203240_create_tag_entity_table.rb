# frozen_string_literal: true

class CreateTagEntityTable < ActiveRecord::Migration[6.0]
  def change
    create_table :tagging do |t|
      t.references :taggable, polymorphic: true
      t.references :tag
      t.timestamps
    end

    drop_table :features_tags # rubocop:disable Rails/ReversibleMigration
    drop_table :scenarios_tags # rubocop:disable Rails/ReversibleMigration
  end
end
