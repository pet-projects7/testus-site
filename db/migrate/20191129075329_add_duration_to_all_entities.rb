# frozen_string_literal: true

# frozen_string_literal: true.

class AddDurationToAllEntities < ActiveRecord::Migration[6.0]
  def change
    add_column :features, :status, :integer
    add_column :features, :duration, :string

    add_column :scenarios, :status, :integer
    add_column :scenarios, :duration, :string
  end
end
