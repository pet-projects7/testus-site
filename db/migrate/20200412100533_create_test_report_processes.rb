# frozen_string_literal: true

class CreateTestReportProcesses < ActiveRecord::Migration[6.0]
  def change
    create_table :test_report_processes do |t|
      t.belongs_to :test_report, null: false, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end
