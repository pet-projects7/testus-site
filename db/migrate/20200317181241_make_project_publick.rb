# frozen_string_literal: true

class MakeProjectPublick < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :public, :boolean, default: false
  end
end
