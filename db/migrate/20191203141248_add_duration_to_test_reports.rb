# frozen_string_literal: true

class AddDurationToTestReports < ActiveRecord::Migration[6.0]
  def change
    change_column :test_reports, :duration, :string
  end
end
