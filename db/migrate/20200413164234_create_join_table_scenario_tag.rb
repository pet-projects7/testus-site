# frozen_string_literal: true

class CreateJoinTableScenarioTag < ActiveRecord::Migration[6.0]
  def change
    create_join_table :scenarios, :tags, id: false do |t|
      # t.index [:scenario_id, :tag_id]
      # t.index [:tag_id, :scenario_id]
    end
  end
end
