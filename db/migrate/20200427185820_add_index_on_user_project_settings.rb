# frozen_string_literal: true

class AddIndexOnUserProjectSettings < ActiveRecord::Migration[6.0]
  def change
    add_index :user_project_settings, %i[name project_id user_id], unique: true
  end
end
