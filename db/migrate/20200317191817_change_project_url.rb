# frozen_string_literal: true

class ChangeProjectUrl < ActiveRecord::Migration[6.0]
  def change
    change_column :projects, :url, :text
    rename_column :projects, :url, :description
  end
end
