# frozen_string_literal: true

class CreateJoinTableFeatureTag < ActiveRecord::Migration[6.0]
  def change
    create_join_table :features, :tags, id: false do |t|
      # t.index [:feature_id, :tag_id]
      # t.index [:tag_id, :feature_id]
    end
  end
end
