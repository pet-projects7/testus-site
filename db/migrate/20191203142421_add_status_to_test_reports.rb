# frozen_string_literal: true

class AddStatusToTestReports < ActiveRecord::Migration[6.0]
  def change
    change_column :features, :status, :string
    add_column :test_reports, :status, :string
  end
end
