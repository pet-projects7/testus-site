# frozen_string_literal: true

class CreateSteps < ActiveRecord::Migration[6.0]
  # rubocop:disable Metrics/MethodLength
  def change
    create_table :steps do |t|
      t.integer :cucumber_type
      t.string :keyword
      t.string :name
      t.integer :line
      t.string :location
      t.integer :status
      t.string :duration
      t.belongs_to :scenario, null: false, foreign_key: true

      t.timestamps
    end
  end
  # rubocop:enable Metrics/MethodLength
end
