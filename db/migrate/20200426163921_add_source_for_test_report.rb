# frozen_string_literal: true

class AddSourceForTestReport < ActiveRecord::Migration[6.0]
  def change
    add_column :test_reports, :source, :integer
  end
end
