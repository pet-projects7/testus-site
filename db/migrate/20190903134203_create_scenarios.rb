# frozen_string_literal: true

class CreateScenarios < ActiveRecord::Migration[6.0]
  def change
    create_table :scenarios do |t|
      t.string :cucumber_id
      t.string :keyword
      t.string :name
      t.string :description
      t.string :line
      t.string :cucumber_type
      t.belongs_to :feature, null: false, foreign_key: true

      t.timestamps
    end
  end
end
