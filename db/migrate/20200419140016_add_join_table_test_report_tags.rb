# frozen_string_literal: true

class AddJoinTableTestReportTags < ActiveRecord::Migration[6.0]
  def change
    create_join_table :test_reports, :tags, id: false do |t|
    end
  end
end
